<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','6500M');
set_time_limit ( 60 * 60 ) ; // Seconds
include_once ( "php/common.php" ) ;

$minq = 10 ;

print get_common_header ( '' , 'Sexer' ) ;
print "<div>Getting all first names of people without gender on Wikidata... (this will take a minute or so)</div>" ; myflush() ;

$j = json_decode ( file_get_contents ( "$wdq_internal_url?q=claim[31:5]%20AND%20noclaim[21]" ) ) ;

$db = openDB ( 'wikidata' , 'wikidata' ) ;

$sql = "select term_entity_id,group_concat(DISTINCT term_text,CHAR(1) separator '|') AS label FROM wb_terms where  term_type='label' and term_entity_type='item' and term_entity_id in (" . join(',',$j->items) . ") group by term_entity_id" ;
// term_language IN ('de','en','es','fr','it') AND
unset ( $j ) ;

$name2q = array() ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$a = explode ( '|' , $o->label ) ;
	foreach ( $a AS $name ) {
		$name = preg_replace ( '/_/' , ' ' , $name ) ;
		$name = preg_replace ( '/^death of /i' , '' , $name ) ;
		$name = preg_replace ( '/^lord /i' , '' , $name ) ;
		if ( preg_match ( '/\bsaint\b/' , $name ) ) continue ;
		if ( preg_match ( '/\b[au]nd\b/' , $name ) ) continue ;
		if ( preg_match ( '/\bMrs\.\b/' , $name ) ) continue ;
		if ( preg_match ( '/\bLady\b/' , $name ) ) continue ;
		if ( preg_match ( '/\bdiscography\b/i' , $name ) ) continue ;
		$name = explode ( ' ' , $name ) ;
		while ( count($name) > 1 ) {
			if ( preg_match ( '/\.$/' , $name[0] ) ) array_shift ( $name ) ;
			else break ;
		}
		if ( count ( $name ) < 2 ) continue ;
//		$name2q[$name[0]][$o->term_entity_id] = $o->term_entity_id ;
		$name2q[$name[0]][] = $o->term_entity_id ;
	}
}

print "<div>" . count($name2q) . " first names in total.</div>" ; myflush() ;

$n2q = array() ;
foreach ( $name2q AS $k => $v ) {
	$v = array_unique ( $v ) ;
	if ( count ( $v ) < $minq ) continue ;//unset ( $name2q[$k] ) ;
	$n2q[$k] = $v ;
}
unset ( $name2q ) ;

print "<div>" . count($n2q) . " names with >= $minq items.</div>" ;

function cmp($a, $b) {
	if ( count($a) < count($b) ) return 1 ;
	if ( count($a) > count($b) ) return -1 ;
	return 0 ;
}

uasort($n2q, 'cmp');

print "<div><i>Note: </i> sex/gender property is P21, male is Q6581097, female is Q6581072</div>" ;

print "<hr/><ul>" ;
myflush();

$sure_male = '' ;
$sure_female = '' ;

foreach ( $n2q AS $name => $qs ) {
	$n = $db->real_escape_string ( $name ) ;
//	$sql = "select pl_title,count(distinct pl_from) AS cnt from pagelinks,wb_entity_per_page,wb_terms WHERE pl_from=epp_page_id AND term_type='label' and term_entity_type='item' AND term_text LIKE '$n %' AND epp_entity_id=term_entity_id AND pl_namespace=0 AND pl_title IN ('Q6581097','Q6581072') GROUP BY pl_title" ;
//	$sql = "select pl_title,count(distinct pl_from) AS cnt from pagelinks,wb_entity_per_page WHERE pl_from=epp_page_id AND epp_entity_id in (select distinct term_entity_id from wb_terms WHERE term_type='label' and term_entity_type='item' AND term_text LIKE '$n %') AND pl_namespace=0 AND pl_title IN ('Q6581097','Q6581072') GROUP BY pl_title" ;
	$pages = array() ;
//	$sql = "select distinct epp_page_id from wb_entity_per_page,wb_terms WHERE term_type='label' and term_entity_type='item' AND term_text LIKE '$n %' AND epp_entity_id=term_entity_id" ;
	$sql = "select distinct epp_page_id from wb_entity_per_page WHERE epp_entity_id IN (select distinct term_entity_id from wb_terms WHERE term_type='label' and term_entity_type='item' AND term_text LIKE '$n %')" ;
//	print "<li>$sql</li>" ; myflush() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$pages[] = $o->epp_page_id ;
	}
	if ( count($pages) == 0 ) continue ;

	$sql = "select pl_title,count(distinct pl_from) AS cnt from pagelinks where pl_from in (" . implode(',',$pages) . ") AND pl_title IN ('Q6581097','Q6581072') GROUP BY pl_title" ;
//	print "<li>$sql</li>" ; myflush() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$male = 0 ;
	$female = 0 ;
	while($o = $result->fetch_object()){
		if ( $o->pl_title == 'Q6581097' ) $male = $o->cnt ;
		if ( $o->pl_title == 'Q6581072' ) $female = $o->cnt ;
	}
//	print "<li>!!</li>" ; myflush() ;
	myflush() ;
	
	if ( ( $male >= 40 and $female == 0 ) or ( $male >= 100 and $female < 3 ) or ( $male >= 1000 and $female < 10 ) or ( $male >= 10000 and $female < 30 ) ) $sure_male .= "\nQ" . implode("\nQ",$qs) ;
	if ( ( $female >= 40 and $male == 0 ) or ( $female >= 100 and $male < 3 ) or ( $female >= 1000 and $male < 10 ) or ( $female >= 10000 and $male < 30 ) ) $sure_female .= "\nQ" . implode("\nQ",$qs) ;

	if ( $male == 0 ) $male = "<span style='color:red'>$male</span>" ;
	if ( $female == 0 ) $female = "<span style='color:red'>$female</span>" ;

	print "<li>" ;
	print "<div><b>$name</b> (" . count($qs) . " items)" ;
	print " <a href='#' onclick='$($(this).parents(\"li\")[0]).find(\"textarea\").toggle();return false'>show/hide items</a> Other items with that name: $male ♂/$female ♀</div>" ;
	print "<textarea style='display:none'>Q" . implode("\nQ",$qs) . "</textarea>" ;
	print "</li>" ;
	myflush() ;
}

print "</ul>" ;

print "<div>These are most likely men:<br/><textarea>$sure_male</textarea></div>" ;
print "<div>These are most likely women:<br/><textarea>$sure_female</textarea></div>" ;


//print "<pre>" ; print_r ( $n2q ) ; print "</pre>" ;

print get_common_footer() ;

?>