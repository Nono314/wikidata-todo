<?PHP

#error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
#ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

header('Content-type: text/plain');

include_once ( 'php/common.php' ) ;

$q = $_REQUEST['q'] ;

$q = str_replace ( ' ' , '%20' , $q ) ;
$wdq = json_decode ( file_get_contents ( $wdq_internal_url."?q=$q&props=625" ) ) ;

$o = '' ;
$o->{'type'} = 'FeatureCollection' ;
$o->{'features'} = array() ;
foreach ( $wdq->props->{'625'} AS $id => $d ) {
	$coord = explode ( '|' , $d[2] ) ;

	$c = '' ;
	$c->id = $id ;
	$c->geometry = '' ;
	$c->geometry->coordinates = array ( $coord[1] , $coord[0] ) ;
	$c->geometry->{'type'} = 'Point' ;
	$c->properties = '' ;
	$c->properties->accuracy = 1 ; // Dummy
	$c->properties->{'duration'} = 0 ; // Dummy
	$c->properties->placename = 'Q'.$d[0] ;
	$o->{'features'}[] = $c ;
}


//header('Content-type: application/json');
print json_encode ( $o ) ;


?>