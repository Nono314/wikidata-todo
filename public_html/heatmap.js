
$(document).ready ( function () {

function getUrlVars () {
	var vars = {} ;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1) ;
	var hash = window.location.href.slice(window.location.href.indexOf('#') + 1) ;
	if ( hash == window.location.href ) hash = '' ;
	if ( hash.length > 0 ) hashes = hash ;
	else hashes = hashes.replace ( /#$/ , '' ) ;
	hashes = hashes.split('&');
	$.each ( hashes , function ( i , j ) {
		var hash = j.split('=');
		hash[1] += '' ;
		vars[hash[0]] = decodeURI(hash[1]).replace(/_/g,' ');
	} ) ;
	return vars;
}


var params = getUrlVars() ;
var region = (params.region||'Q23112').replace(/\D/g,'') ; // 145=UK; Cambridgeshire fallback
$('#region').text('Q'+region).attr({href:'//tools.wmflabs.org/reasonator/?q=Q'+region}) ;
$('#autolist').attr({href:"//tools.wmflabs.org/wikidata-todo/autolist.html?props=625&q=tree["+region+"][][131] AND noclaim[18] AND claim[625]"});
$('#wdfist').attr({href:"//tools.wmflabs.org/fist/wdfist/index.html?wdq=tree["+region+"][][131] AND noclaim[18] AND claim[625]"});
$('input[name=region]').val('Q'+region);

var raster = new OpenLayers.Layer.OSM("osm");

var vector = new OpenLayers.Layer.Vector("heatmap", {
// use the heatmap renderer instead of the default one (SVG, VML or Canvas)
    renderers: ['Heatmap'],
    protocol: new OpenLayers.Protocol.HTTP({
        url: "coords2json.php?q=tree["+region+"][][131] AND noclaim[18] AND claim[625]",
        format: new OpenLayers.Format.GeoJSON()

    }),
    styleMap: new OpenLayers.StyleMap({
        "default": new OpenLayers.Style({
            pointRadius: 10,
            weight: "${weight}"
        }, {
            context: {
// the 'weight' of the point (between 0.0 and 1.0), used by the heatmap renderer
                weight: function(f) {
					var zoom = f.layer.map.zoom ;
					var levels = f.layer.numZoomLevels ;
                if(params.test!==undefined) console.log(zoom/levels);
                    return Math.min(Math.max(zoom/levels,0.1),1) ;
//                    return Math.min(Math.max((f.attributes.duration || 0) / 100000, 0.25), 1.0);
                }
            }
        })
    }),
    strategies: [new OpenLayers.Strategy.Fixed()],
    eventListeners: {
        featuresadded: function(evt) {
            this.map.zoomToExtent(this.getDataExtent());
        }
    }
});

var markers = new OpenLayers.Layer.Markers( "Markers" );


var map = new OpenLayers.Map("map", {
    layers: [raster, markers, vector]
});


var has_markers = false ;
/*
function zoomChanged () {
	zoom = map.getZoom();
	if ( zoom >= 15 ) { // Show markers

		markers.setVisibility ( true ) ;

	} else { // Hide markers
//		markers.setVisibility ( false ) ;
	}
}

map.events.register("zoomend", map, zoomChanged);
*/
markers.setVisibility ( false ) ;

function loadMarkers () {
	if ( has_markers ) return ;
	has_markers = true ;
	var size = new OpenLayers.Size(21,25);
	var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
	var icon = new OpenLayers.Icon('resources/js/openlayers/img/marker.png',size,offset);
	
	$('#loading_markers').show() ;
	$.getJSON ( "//wdq.wmflabs.org/api?q=tree["+region+"][][131] AND noclaim[18] AND claim[625]&props=625&callback=?" , function ( d ) {
		$.each ( d.props['625'] , function ( k , v ) {
			var q = 'Q'+v[0] ;
			var pos = v[2].split(/\|/) ;
			var lonlat = new OpenLayers.LonLat(pos[1]*1,pos[0]*1) ;
			lonlat = lonlat.transform(
                            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                            map.getProjectionObject() // to Spherical Mercator Projection
                            );
                            
			var marker = new OpenLayers.Marker(lonlat, icon.clone() ) ;
			marker.id = q;
			marker.events.register("click", marker, function(evt) {
				var url = "//www.wikidata.org/wiki/"+this.id ;
				window.open (url,"_blank");
			});
			marker.events.register("mouseover", marker, function(evt) {
				$('#q_hover').remove() ;
				$('h1').append("<span id='q_hover'> [last marker: <a target='_blank' href='//www.wikidata.org/wiki/"+this.id+"'>"+this.id+"</a>]</span>") ;
			});
//			marker.events.register("mouseout", marker, function(evt) {} ) ;
                            
			markers.addMarker(marker);

		} )
		$('#loading_markers').remove() ;
//		markers.setVisibility ( true ) ;
	} ) ;

}

/*
function markerClick ( evt ) {
//				console.log ( evt.events.id ) ;
//	console.log ( evt ) ;
}
markers.events.register("mousedown", markers, markerClick);
*/

var markers_visible = false ;

function toggleMarkers () {
	loadMarkers() ;
	if ( markers_visible ) markers.setVisibility ( false ) ;
	else markers.setVisibility ( true ) ;
	markers_visible = !markers_visible ;
}

markers.setZIndex( 1001 ); 

$('#loadMarkers').click ( function () {
	toggleMarkers() ;
	return false ;
} ) ;

} ) ;
